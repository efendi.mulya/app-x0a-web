<?php
	include "show_data.php";
?>
<html>
<html>
	<head>
		<title>Index</title>
	</head>
	<body>
		<header>
			<h1>Data Mahasiswa</h1>
		</header>		
		<br>
		<table border="1">
			<thead>
				<tr>
					<th>NIM</th>
					<th>Nama Mahasiswa</th>
					<th>Program Studi</th>
                    <th>Alamat</th>
					<th>Jenis Kelamin</th>										
                    <th>URL</th>
					<th>Foto</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$sql = "SELECT m.nim, m.nama, p.nama_prodi, CONCAT('http://localhost/app-x0a-web/images/',photos) AS url, 
                            m.alamat, m.jenis_kelamin
                            FROM mahasiswa m, prodi p
                            WHERE m.id_prodi = p.id_prodi";
					$query = mysqli_query($conn, $sql);
					while($data = mysqli_fetch_array($query)){
						echo "<tr>";
						echo "<td><center>".$data['nim']."</center></td>";
                        echo "<td><center>".$data['nama']."</center></td>";
                        echo "<td><center>".$data['nama_prodi']."</center></td>";
						echo "<td><center>".$data['alamat']."</center></td>";
                        echo "<td><center>".$data['jenis_kelamin']."</center></td>";
                        echo "<td><center>".$data['url']."</center></td>";        						
						echo "<td><center><img width='70px' src='".$data['url']."'></center></td>";						
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
		<br>
		<h1>Data Program Studi</h1>		
		<br>
		<table border="1">
			<thead>
				<tr>
					<th>ID Prodi</th>
					<th>Nama Program Studi</th>					
				</tr>
			</thead>
			<tbody>
				<?php
					$sql2 = "SELECT *
                            FROM prodi";
					$query2 = mysqli_query($conn, $sql2);
					while($data2 = mysqli_fetch_array($query2)){
						echo "<tr>";
						echo "<td><center>".$data2['id_prodi']."</center></td>";
                        echo "<td><center>".$data2['nama_prodi']."</center></td>";                        
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</body>
</html>