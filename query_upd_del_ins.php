<?php
    $DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array(); $respon['kode'] = '000';
        switch($mode){
            case "insert":
                $nim = $_POST["nim"];
                $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
                $nama_prodi = $_POST["nama_prodi"];
                $jenis_kelamin = $_POST["jenis_kelamin"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$alamat = $_POST["alamat"];
                $path = "images/";


                $sql = "SELECT id_prodi from prodi where nama_prodi='$nama_prodi'";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $id_prodi = $data['id_prodi'];
                    $sql = "INSERT into mahasiswa(nim, nama, id_prodi, photos, alamat, jenis_kelamin) values(
                        '$nim','$nama','$id_prodi','$file', '$alamat', '$jenis_kelamin')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $sql = "delete from mahasiswa where nim='$nim'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            echo json_encode($respon); exit();
                        }
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
            case "update":
                $nim = $_POST["nim"];
                $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
                $nama_prodi = $_POST["nama_prodi"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
				$alamat = $_POST["alamat"];
                $path = "images/";
                $jenis_kelamin = $_POST["jenis_kelamin"];


                $sql = "SELECT id_prodi from prodi where nama_prodi='$nama_prodi'";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $id_prodi = $data['id_prodi'];

                    $sql = "";
                    if($imstr==""){
                        $sql = "UPDATE mahasiswa SET nama='$nama',id_prodi=$id_prodi, alamat='$alamat', jenis_kelamin='$jenis_kelamin'
                        where nim='$nim'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }else{                                                
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            $sql = "UPDATE mahasiswa SET nama='$nama',id_prodi=$id_prodi,photos='$file', alamat='$alamat', jenis_kelamin='$jenis_kelamin'
                                    where nim='$nim'";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                echo json_encode($respon); exit(); //update data sukses semua
                            }else{
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }
                        }

                        }                                                                                
                }
            break;
            case "delete":
                $nim = $_POST["nim"];
                $sql = "SELECT photos from mahasiswa where nim='$nim'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['photos'];
                        $path = "images/";
                        unlink($path.$photos);
                    }
                    $sql = "DELETE from mahasiswa where nim='$nim'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit(); //delete data sukses
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
        }
    }
?>